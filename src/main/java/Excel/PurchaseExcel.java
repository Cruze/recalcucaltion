package Excel;

import CalculateSystem.Items.Item;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cruze_000 on 14.03.2015.
 * this class for work with purchase excel
 */
public class PurchaseExcel extends AbstractExcel {
    public PurchaseExcel(File file) {
        super(file);
    }

    @Override
    public List<Item> getItems() {
        List<Item> items = new ArrayList<Item>();


        Sheet sheet = workbook.getSheetAt(0);

        for (Row row : sheet) {

            String name = getName(row.getCell(0));

            int pricePurchase = getPrice(row.getCell(5));

            boolean exist = getBoolean(row.getCell(1));

            if (!exist)
                pricePurchase = 0;

            items.add(new Item(name, pricePurchase));
        }

        return items;
    }

    @Override
    public void write(List<Item> resultItems) {

    }
}
