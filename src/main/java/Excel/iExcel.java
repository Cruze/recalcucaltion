package Excel;

import java.io.File;

/**
 * Created by Cruze_000 on 13.03.2015.
 * excel interface.
 */
public interface iExcel {

    public void setFile(File file);

    public void save();

    public String getUrl();


}
