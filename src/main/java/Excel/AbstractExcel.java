package Excel;

import CalculateSystem.Items.Item;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created by Cruze on 11.03.2015.
 * this class for work with AbstractExcel table
 */
public abstract class AbstractExcel {

    File file = null;
    String url = "";

    HSSFWorkbook workbook = null;


    public AbstractExcel(File file) {
        setFile(file);
    }

    protected AbstractExcel() {
    }

    public String getUrl() {
        return url;
    }


    public void setFile(File file) {
        this.url = file.getAbsolutePath();
        this.file = file;
        setWorkbook(file);
    }

    private void setWorkbook(File file) {
        try {
            this.workbook = new HSSFWorkbook(new FileInputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
            //TODO обработать ошибку
        }
    }

    protected String getName(Cell cell) {
        String name;
        name = cell.getStringCellValue();
        return name;
    }

    protected int getPrice(Cell cell) {

        if (cell == null)
            return 0;
        if (cell.getCellType() != Cell.CELL_TYPE_NUMERIC)
            return 0;
        if (cell.getNumericCellValue() != 0) {
            return (int) cell.getNumericCellValue();
        }
        return 0;
    }

    protected boolean getBoolean(Cell cell){


        if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN)
            return cell.getBooleanCellValue();

        String string = cell.getStringCellValue();
        return !(string.isEmpty() || string.equals("0"));

    }

    public void save() {
        try {
            FileOutputStream out = new FileOutputStream(file);
            workbook.write(out);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public abstract List<Item> getItems();

    public abstract void write(List<Item> resultItems);

}
