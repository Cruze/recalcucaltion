package Excel;

import CalculateSystem.Items.Item;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cruze_000 on 14.03.2015.
 * this class for work with monitoring excel
 */
public class MonitoringExcel extends AbstractExcel {

    public MonitoringExcel(File file) {
        super(file);
    }

    @Override
    public List<Item> getItems() {

        List<Item> items = new ArrayList<Item>();

        if(workbook == null)
            return items;


        Sheet sheet = workbook.getSheetAt(0);
        for (Row row : sheet) {
            String name = getName(row.getCell(0));

            Item item = new Item(name);

            item.setPriceConcurrent(getPrice(row.getCell(1)));

            item.setPriceMinimum(getPrice(row.getCell(2)));

            items.add(item);
        }

        return items;
    }



    @Override
    public void write(List<Item> resultItems) {

    }
}
