package Excel;

import CalculateSystem.Items.Item;
import CalculateSystem.Items.ItemColor;
import CalculateSystem.Parametres.iParametres;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Cruze_000 on 14.03.2015.
 * this class is result excel
 */
public class ResultExcel extends AbstractExcel {

    private HashMap<Integer, HSSFCellStyle> _styles = new HashMap<Integer, HSSFCellStyle>();
    iParametres parametres;

    public ResultExcel(File file, iParametres parametres) {
        this.file = file;
        this.parametres = parametres;
    }

    @Override
    public List<Item> getItems() {
        return null;
    }

    @Override
    public void write(List<Item> resultItems) {
        workbook = new HSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        HSSFCellStyle style;
        int rowcount = 0;

        for (Item item : resultItems) {

            style = getStyleForItem(item);

            if (item.getItemColors().size() == 0){
                String name = item.getName();

                Row row = sheet.createRow(rowcount++);
                Cell cell = row.createCell(0);
                cell.setCellValue(name);
                cell.setCellStyle(style);

                cell = row.createCell(1);
                cell.setCellValue(item.getPrice());
                cell.setCellStyle(style);

                cell = row.createCell(2);
                cell.setCellValue(item.getPricePurchase());
                cell.setCellStyle(style);


                cell = row.createCell(3);
                cell.setCellValue(item.getPriceConcurrent());
                cell.setCellStyle(style);

                cell = row.createCell(4);
                cell.setCellValue(item.getPriceMinimum());
                cell.setCellStyle(style);

                cell = row.createCell(5);
                cell.setCellStyle(style);
                if (item.isDifferentPurchasePrice())
                    cell.setCellValue("Разная цена закупки");

            } else {

                for (ItemColor color : item.getItemColors()) {
                    String name = item.getName() + " " + color.toString().toLowerCase().replaceAll("_", "'");

                    Row row = sheet.createRow(rowcount++);
                    Cell cell = row.createCell(0);
                    cell.setCellValue(name);
                    cell.setCellStyle(style);

                    cell = row.createCell(1);
                    cell.setCellValue(item.getPrice());
                    cell.setCellStyle(style);

                    cell = row.createCell(2);
                    cell.setCellValue(item.getPricePurchase());
                    cell.setCellStyle(style);


                    cell = row.createCell(3);
                    cell.setCellValue(item.getPriceConcurrent());
                    cell.setCellStyle(style);

                    cell = row.createCell(4);
                    cell.setCellValue(item.getPriceMinimum());
                    cell.setCellStyle(style);

                    cell = row.createCell(5);
                    cell.setCellStyle(style);
                    if (item.isDifferentPurchasePrice())
                        cell.setCellValue("Разная цена закупки");
                }
            }

        }
    }

    private HSSFCellStyle getStyleForItem(Item item) {
        HSSFCellStyle style;
        int index = getIndexColour(item);

        style = _styles.get(index);

        if (style == null) {
            style = workbook.createCellStyle();
            style.setFillForegroundColor((short) index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            _styles.put(index, style);
        }

        return style;
    }

    private int getIndexColour(Item item) {
        int index = HSSFColor.WHITE.index;

        if (item.getPricePurchase() == 0)
            return index;

        if (item.isMinimumPrice())
           return HSSFColor.LIGHT_GREEN.index;

        int pricePurchaseWithCommission = item.getPricePurchase() + getCommission(item.getPricePurchase());

        if (item.getPriceConcurrent() == 0){
            return index;
        }

        if (item.getPriceConcurrent() - pricePurchaseWithCommission > parametres.getTopMargin())
            return HSSFColor.GREEN.index;

        if (item.getPriceConcurrent() - pricePurchaseWithCommission < parametres.getBottomMargin())
            return HSSFColor.ORANGE.index;
        return index;
    }

    private int getCommission(int price) {
        //Формируем комиссию
        int commission = (price / 100) * parametres.getCommissionPercentage();
        //если комиссия получилась меньше минимальной комиссии, то берем минимальную комиссию
        if (commission < parametres.getCommissionMin())
            commission = parametres.getCommissionMin();
        return commission;
    }


}
