package CalculateSystem.Parametres;

/**
 * Created by Cruze_000 on 13.03.2015.
 * reilize iParametres inreface
 */
public class Parametres implements iParametres{

    private int bottomMargin;
    private int topMargin;
    private int commissionPercentage;
    private int commissionMin;
    private boolean ConcurentPriceConsider;

    @Override
    public int getBottomMargin() {
        return bottomMargin;
    }

    @Override
    public void setBottomMargin(int bottomMargin) {
        this.bottomMargin = bottomMargin;
    }

    @Override
    public int getTopMargin() {
        return topMargin;
    }

    @Override
    public void setTopMargin(int topMargin) {
        this.topMargin = topMargin;
    }

    @Override
    public int getCommissionPercentage() {
        return commissionPercentage;
    }

    @Override
    public void setCommissionPercentage(int commissionPercentage) {
        this.commissionPercentage = commissionPercentage;
    }

    @Override
    public int getCommissionMin() {
        return commissionMin;
    }



    @Override
    public void setCommissionMin(int commissionMax) {
        this.commissionMin = commissionMax;
    }

    public boolean isConcurentPriceConsider() {
        return ConcurentPriceConsider;
    }

    @Override
    public void setConcurentPriceConsider(boolean concurentPriceConsider) {
        ConcurentPriceConsider = concurentPriceConsider;
    }
}
