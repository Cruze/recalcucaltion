package CalculateSystem.Parametres;

/**
 * Created by Cruze_000 on 13.03.2015.
 * parametres and settings for calculate system
 */
public interface iParametres {

    public void setBottomMargin(int bottomMargin);
    public void setTopMargin(int topMargin);
    public void setCommissionPercentage(int commissionPercentage);
    public void setCommissionMin(int commissionMax);
    public void setConcurentPriceConsider(boolean concurentPriceConsider);

    public int getBottomMargin();
    public int getTopMargin();
    public int getCommissionPercentage();
    public int getCommissionMin();
    public boolean isConcurentPriceConsider();




}
