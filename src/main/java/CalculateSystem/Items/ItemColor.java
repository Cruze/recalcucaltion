package CalculateSystem.Items;

/**
 * Created by Cruze_000 on 14.03.2015.
 * colour of item
 */
public enum ItemColor {
    // difficult colors
    BLACK_BLUE("black\\s*'\\s*blue"),
    BLACK_SILVER("black\\s*'\\s*silver"),
    BLACK_GREEN("black\\s*'\\s*green"),
    BLACK_ORANGE("black\\s*'\\s*orange"),
    BROWN_RED("brown\\s*'\\s*red"),
    DARK_GREY("dark grey"),
    // simple colors
    BLACK("черн|black"),
    BRONZE,
    ILLUVIAL,
    GOLD,
    GREY,
    BORDO,
    PINK,
    BEIGE,
    WHITE("бел|white"),
    RED,
    SILVER,
    BROWN,
    YELLOW,
    BLUE,
    GREEN,
    ORANGE,
    PURPLE,
    CHROME,
    LILAC,
    VIOLET,
    KHAKI;

    private String _regExp;

    ItemColor(String name) {
        _regExp = name;
    }

    ItemColor() {
        _regExp = toString().toLowerCase();
    }

    public String getRegExp() {
        return _regExp;
    }

}
