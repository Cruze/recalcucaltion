package CalculateSystem.Items;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Cruze on 29.03.2015.
 * list of monitoring Items
 */
public class MonitoringItems {

    private List<Item> items = new ArrayList<Item>();

    public MonitoringItems(List<Item> items) {
        this.items = compact(items);
    }

    private List<Item> compact(List<Item> items) {
        List<Item> newItems = new ArrayList<Item>();
        //пробегаем весь список
        for (int i = 0; i < items.size(); i++) {
            Item itemA = items.get(i);
            //пробегаем весь оставшийся список
            Item item = new Item(itemA);
            for (int j = i + 1; j < items.size(); j++) {
                Item itemB = items.get(j);
                // если нашли два товара с одинаковым названием
                if (itemA.equals(itemB)) {
                    Set<ItemColor> colors = item.getItemColors();
                    int minimumPrice = item.getPriceMinimum();

                    int concurrentPrice = item.getPriceConcurrent();

                    item = mergeItems(itemA, itemB);

                    if (item.getPriceConcurrent() == 0 || item.getPriceConcurrent() > concurrentPrice)
                        item.setPriceConcurrent(concurrentPrice);

                    item.setPriceMinimum(minimumPrice);

                    if (itemB.isMinimumPrice()) {
                        if (!item.isMinimumPrice())
                        item.setPriceMinimum(itemB.getPriceMinimum());
                        else
                            if (item.getPriceMinimum() > itemB.getPriceMinimum())
                                item.setPriceMinimum(itemB.getPriceMinimum());
                    }


                    colors.addAll(itemB.getItemColors());
                    item.addItemColors(colors);
                }
            }
            //если товара ещё нет в списке, то добдавляем
            if (!newItems.contains(item))
                newItems.add(item);
        }
        return newItems;
    }

    private Item mergeItems(Item itemA, Item itemB) {
        Item item = new Item(itemA.getName());


        item.setPriceConcurrent( getPriceConcurent(itemA, itemB));


        item.setPriceMinimum(getMinimumPrice(itemA, itemB));

        return item;
    }

    private int  getPriceConcurent(Item itemA, Item itemB) {
        if (itemA.getPriceConcurrent() == 0)
            return itemB.getPriceConcurrent();
        if (itemB.getPriceConcurrent() == 0)
            return itemB.getPriceConcurrent();

        if (itemA.getPriceConcurrent() < itemB.getPriceConcurrent())
            return itemA.getPriceConcurrent();
        else
           return itemB.getPriceConcurrent();
    }

    private int getMinimumPrice(Item itemA, Item itemB) {

        if (itemA.getPriceMinimum() == 0 && itemB.getPriceMinimum() == 0)
            return 0;

        if (itemA.getPriceMinimum() == 0)
            return itemB.getPriceMinimum();

        if (itemB.getPriceMinimum() == 0)
            return itemA.getPriceMinimum();

        int price;

        if (itemA.getPriceMinimum() < itemB.getPriceMinimum())
            price = itemA.getPriceMinimum();
        else
            price = itemB.getPriceMinimum();

        return price;
    }

    public boolean contains(Item purchaseItem) {
        return items.contains(purchaseItem);
    }

    public int indexOf(Item purchaseItem) {
        return items.indexOf(purchaseItem);
    }

    public Item get(int index) {
        return items.get(index);
    }

    public List<Item> getItems() {
        return items;
    }
}
