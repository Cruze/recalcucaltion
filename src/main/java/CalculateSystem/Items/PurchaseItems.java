package CalculateSystem.Items;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by Cruze on 29.03.2015.
 * List of Purchase Items
 */
public class PurchaseItems {

    private List<Item> items = new ArrayList<Item>();

    public PurchaseItems(List<Item> items) {
        this.items = compact(items);
    }

    private List<Item> compact(List<Item> items) {
        //Создаем новый список, в котором товары будут уникальны
        List<Item> newItems = new ArrayList<Item>();
        //пробегаем весь список
        for (int i = 0; i < items.size(); i++) {
            Item itemA = items.get(i);
            //пробегаем весь оставшийся список
            Item item = new Item(itemA);
            for (int j = i + 1; j < items.size(); j++) {
                Item itemB = items.get(j);
                // если нашли два товара с одинаковым названием
                if (itemA.equals(itemB)) {
                    Set<ItemColor> colours = item.getItemColors();

                    int pricePurchase = item.getPricePurchase();

                    item = mergeItems(itemA, itemB);

                    if (pricePurchase > item.getPricePurchase())
                        item.setPricePurchase(pricePurchase);
                    colours.addAll(itemB.getItemColors());
                    item.addItemColors(colours);


                }
            }
            //если товара ещё нет в списке, то добдавляем
            if (!newItems.contains(item))
                newItems.add(item);
        }
        return newItems;
    }

    private Item mergeItems(Item itemA, Item itemB) {
        Item item = new Item(itemA.getName());
        item.setDifferentPurchasePrice(isDifferentPricePurchase(itemA, itemB));

        if (itemA.getPricePurchase() > itemB.getPricePurchase())
            item.setPricePurchase(itemA.getPricePurchase());
        else
            item.setPricePurchase(itemB.getPricePurchase());

        return item;
    }

    private boolean isDifferentPricePurchase(Item itemA, Item itemB) {
        return itemA.getPricePurchase() != itemB.getPricePurchase();
    }

    public List<Item> getItems() {
        return items;
    }
}
