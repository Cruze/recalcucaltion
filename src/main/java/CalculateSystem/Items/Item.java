package CalculateSystem.Items;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Cruze_000 on 14.03.2015.
 * it is one item reilize iItem
 */
public class Item {
    private static final String NULL = "";

    private String name;

    private int price = 0;

    private int pricePurchase = 0;

    private int priceConcurrent = 0;

    private int priceMinimum = 0;

    private boolean differentPurchasePrice = false;

    private Set<ItemColor> itemColors = new LinkedHashSet<ItemColor>();

    public Item(Item item) {
        this.name = item.getName();
        this.price = item.getPrice();
        this.pricePurchase = item.getPricePurchase();
        this.priceMinimum = item.getPriceMinimum();
        this.priceConcurrent  = item.getPriceConcurrent();
        this.differentPurchasePrice = item.isDifferentPurchasePrice();
        this.itemColors = item.getItemColors();
    }

    public Item(String name, int pricePurchase) {
        this.name = parseName(name);
        this.pricePurchase = pricePurchase;
    }

    public Item(String name) {
        this.name =  parseName(name);
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", colours=" + itemColors +
                '}';
    }

    public String getName() {
        return name;
    }

    public int getPricePurchase() {
        return pricePurchase;
    }

    public Set<ItemColor> getItemColors() {
        return itemColors;
    }

    public void setPricePurchase(int pricePurchase) {
        this.pricePurchase = pricePurchase;
    }

    public boolean isMinimumPrice() {
        return priceMinimum != 0;
    }


    private String parseName(String name) {
        name = name.toLowerCase();
        String colorRegExp;
        String tempName;
        for (ItemColor itemColor : ItemColor.values()) {

            colorRegExp = itemColor.getRegExp();

            tempName = name.replaceAll(colorRegExp, NULL);

            if (!tempName.equals(name)) {
                name = tempName;
                itemColors.add(itemColor);
            }
        }

        name = name.replaceAll(",", NULL);
        name = name.replaceAll("'", NULL).trim();
       return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return name.equals(item.name);

    }
    @Override
    public int hashCode() {
        return name.hashCode();
    }


    public int getPriceMinimum() {
        return priceMinimum;
    }

    public void setPriceConcurrent(int priceConcurrent) {
        this.priceConcurrent = priceConcurrent;
    }

    public int getPriceConcurrent() {
        return priceConcurrent;
    }

    public void setPriceMinimum(int priceMinimum) {
        this.priceMinimum = priceMinimum;
    }

    public boolean isDifferentPurchasePrice() {
        return differentPurchasePrice;
    }

    public void setDifferentPurchasePrice(boolean differentPurchasePrice) {
        this.differentPurchasePrice = differentPurchasePrice;
    }

    public void addItemColors(Set<ItemColor> itemColors){
        this.itemColors.addAll(itemColors);
    }
}


