package CalculateSystem;

import CalculateSystem.Items.Item;
import CalculateSystem.Items.MonitoringItems;
import CalculateSystem.Items.PurchaseItems;
import CalculateSystem.Parametres.iParametres;
import Excel.AbstractExcel;
import Excel.ResultExcel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cruze_000 on 13.03.2015.
 * Calculate System. This system reilize interfiace iCalculateSystem.
 */
public class CalculateSystem implements iCalculateSystem {

    private AbstractExcel purchase;
    private AbstractExcel monitoring;
    private AbstractExcel result;
    private iParametres parametres;

    private List<Item> items = new ArrayList<Item>();

    public CalculateSystem() {
    }

    @Override
    public void setPurchase(AbstractExcel excel) {
        purchase = excel;
    }

    @Override
    public void setMonitoring(AbstractExcel excel) {
        monitoring = excel;
    }


    @Override
    public AbstractExcel getResult() {
        fillResult();
        return result;
    }

    private void fillResult() {
        //Загружает список товаров из excel (закупка) файла. Каждому товару соответствует один цвет
        PurchaseItems purchaseItems = new PurchaseItems(purchase.getItems());

        //Загружает список товаров из excel (мониторинг) файла. Каждому товару соответствует один цвет
        MonitoringItems monitoringItems = new MonitoringItems(monitoring.getItems());


        //Здесь для каждого товара из закупки формируется результирующая цена
        List<Item> resultItems = getResultItems(purchaseItems, monitoringItems);

        items = resultItems;

        File file = new File("result.xls");

        result = new ResultExcel(file, parametres);
        result.write(resultItems);
    }

    private List<Item> getResultItems(PurchaseItems purchaseItems, MonitoringItems monitoringItems) {
        //Учитываем или не учитываем цены конкурентов
        if (parametres.isConcurentPriceConsider()) {
            //Если учитываем
            return getResultItemsWithConcurentPrice(purchaseItems, monitoringItems);
        } else {
            //Если не учитываем
            return getResultItemsWithoutConcurentPrice(purchaseItems);

        }
    }

    private List<Item> getResultItemsWithConcurentPrice(PurchaseItems purchaseItems, MonitoringItems monitoringItems) {
        List<Item> resultItems = new ArrayList<Item>();
        //пробегаем весь список
        for (Item purchaseItem : purchaseItems.getItems()) {
            Item item;
            //если цена закупки равна нулю, то ничего не считаем
            if (purchaseItem.getPricePurchase() == 0) {
                item = purchaseItem;
            } else {
                //если в monitoring'е есть этот товар, то
                if (monitoringItems.contains(purchaseItem)) {
                    //находим итем, который в monitoring'е
                    Item monitoringItem = monitoringItems.get(monitoringItems.indexOf(purchaseItem));
                    //получаем итем из итема закупки и итема мониторинга
                    item = getItemIfMonitoringItemsContains(purchaseItem, monitoringItem);
                } else { // елс и в monitoring'e нет этого товара
                    item = getItemIfMonitoringItemsNotContains(purchaseItem);
                }
            }
            resultItems.add(item);
        }

        return resultItems;
    }

    private Item getItemIfMonitoringItemsContains(Item purchaseItem, Item monitoringItem) {
        Item item = new Item(purchaseItem);

        item.setPriceConcurrent(monitoringItem.getPriceConcurrent());

        item.setPriceMinimum(monitoringItem.getPriceMinimum());
        int price;

        //Формируем новый итем, основываясь на итеме закупки и соответствующем итеме из мониторинга
        if (monitoringItem.isMinimumPrice()) {
            if (monitoringItem.getPriceConcurrent() != 0){

                int different = monitoringItem.getPriceConcurrent() - purchaseItem.getPricePurchase();

                if (different > parametres.getTopMargin())
                    price = purchaseItem.getPricePurchase() + parametres.getTopMargin();
                 else {
                    price = monitoringItem.getPriceConcurrent() - 110;
                    price += getCommission(price);
                }


            } else {
                price = purchaseItem.getPricePurchase() + parametres.getTopMargin();
            }

        } else {

            if (monitoringItem.getPriceConcurrent() != 0){
                price = getPriceFromPurchaceAndMonitoringItem(purchaseItem, monitoringItem);
           //     price += getCommission(price);
            } else {
                price = purchaseItem.getPricePurchase() + parametres.getTopMargin();
            }
        }
        item.setPrice(price);
        return item;
    }

    private Item getItemIfMonitoringItemsNotContains(Item purchaseItem) {
        //если в мониторинге нет этого товара, то
        Item item = new Item(purchaseItem);
        //устанавливаем новую ценку товара
        int price = getPrice(purchaseItem.getPricePurchase());
        price = makeBeautiful(price);
        item.setPrice(price);
        return item;
    }


    private List<Item> getResultItemsWithoutConcurentPrice(PurchaseItems purchaseItems) {
        //Если цены конкурентов не учитываем, то пробегаем весь список
        for (Item purchaseItem : purchaseItems.getItems()) {
            int price = getPrice(purchaseItem.getPricePurchase());
            price = makeBeautiful(price);
            purchaseItem.setPrice(price);
        }
        //возвращаем полученный список товара
        return purchaseItems.getItems();
    }

    private int makeBeautiful(int price) {
        if (price <= 3500)
            return price;

        int modulo = price % 100;

        price += (100 - modulo) - 10;

        return price;
    }

    private int getPrice(int purchaseItemPrice) {
        //берем цену товара
        if (purchaseItemPrice == 0)
            return 0;
        //прибавляем минимальную наценку
        purchaseItemPrice += parametres.getBottomMargin();
        //добавляем комиссию
        purchaseItemPrice += getCommission(purchaseItemPrice);
        //устанавливаем новую ценку товара
        return purchaseItemPrice;
    }


    private int getPriceFromPurchaceAndMonitoringItem(Item purchaseItem, Item monitoringItem) {

            int price = purchaseItem.getPricePurchase();
            price += getCommission(price);

            int different = monitoringItem.getPriceConcurrent() - price;

            if (different > parametres.getTopMargin()) {
                return price + parametres.getTopMargin();
            }
            if(different < parametres.getBottomMargin()){
                return price + parametres.getBottomMargin();
            }
            else {
                if (different <= 200) {
                    return price + parametres.getBottomMargin() - 10;
                } else {
                    return  monitoringItem.getPriceConcurrent() - 100 - 10;
                }
            }

    }


    private int getCommission(int price) {
        //Формируем комиссию
        int commission = (price / 100) * parametres.getCommissionPercentage();
        //если комиссия получилась меньше минимальной комиссии, то берем минимальную комиссию
        if (commission < parametres.getCommissionMin())
            commission = parametres.getCommissionMin();
        return commission;
    }


    @Override
    public void setParametres(iParametres parametres) {
        this.parametres = parametres;
    }

    @Override
    public List<Item> getItems() {
        return items;
    }

}
