package CalculateSystem.GUI;

import CalculateSystem.*;
import CalculateSystem.Parametres.Parametres;
import CalculateSystem.Parametres.iParametres;
import Excel.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by Cruze on 11.03.2015.
 * just main gui form
 */
public class Form extends JFrame{
    private JPanel panel;
    private JButton downloadPurchaseButton;
    private JButton downloadMonitoringButton;
    private JTextField bottomMarginTextField;
    private JTextField topMarginTextField;
    private JTextField commissionPercentageTextField;
    private JTextField commissionMinTextField;
    private JCheckBox concurentPriceCheckBox;
    private JButton calculateButton;
    private JLabel monitoringLabel;
    private JLabel purchaseLabel;

    private iCalculateSystem calculateSystem;

    private iParametres calculateSystemParametres;

    public Form(){
        super("");

        setSettingsForm();

        calculateSystem = new CalculateSystem();
        calculateSystemParametres = new Parametres();

        downloadPurchaseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AbstractExcel excel = new PurchaseExcel(getFile());
                purchaseLabel.setText(excel.getUrl());
                calculateSystem.setPurchase(excel);
            }
        });
        downloadMonitoringButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AbstractExcel excel = new MonitoringExcel(getFile());
                monitoringLabel.setText(excel.getUrl());
                calculateSystem.setMonitoring(excel);
            }
        });

        calculateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setParametres();
                calculateSystem.setParametres(calculateSystemParametres);
                AbstractExcel excel = calculateSystem.getResult();
                excel.save();

                JOptionPane.showMessageDialog(null, "Готово");

            }
        });

    }

    private File getFile() {
        File file = null;
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File("").getAbsoluteFile());
        int ret = fileChooser.showDialog(null, "Выберите файл");
        if (ret == JFileChooser.APPROVE_OPTION){
            file = fileChooser.getSelectedFile();
        }
        return file;
    }

    private void setParametres() {


           int bottomMargin = Integer.parseInt(bottomMarginTextField.getText());
           int topMargin = Integer.parseInt(topMarginTextField.getText());
           int commissionPercentage = Integer.parseInt(commissionPercentageTextField.getText());
           int commissionMin = Integer.parseInt(commissionMinTextField.getText());
           boolean concurentPriceConsider = concurentPriceCheckBox.isSelected();

           calculateSystemParametres.setBottomMargin(bottomMargin);
           calculateSystemParametres.setTopMargin(topMargin);
           calculateSystemParametres.setCommissionPercentage(commissionPercentage);
           calculateSystemParametres.setCommissionMin(commissionMin);
           calculateSystemParametres.setConcurentPriceConsider(concurentPriceConsider);

    }


    private void setSettingsForm() {
        setContentPane(panel);
        pack();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }


}
