package CalculateSystem;

import CalculateSystem.Items.Item;
import CalculateSystem.Parametres.iParametres;
import Excel.AbstractExcel;

import java.util.List;

/**
 * Created by Cruze_000 on 13.03.2015.
 * this system take two excel file and return result excel file by rules
 */
public interface iCalculateSystem {

    public void setPurchase(AbstractExcel excel);

    public void setMonitoring(AbstractExcel excel);

    public AbstractExcel getResult();

    public void setParametres(iParametres parametres);

    public List<Item> getItems();

}
