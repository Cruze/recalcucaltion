package CalculateSystem;

import CalculateSystem.Items.Item;
import CalculateSystem.Parametres.Parametres;
import CalculateSystem.Parametres.iParametres;
import Excel.AbstractExcel;
import Excel.MonitoringExcel;
import Excel.PurchaseExcel;
import org.junit.Test;

import java.nio.file.Paths;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class CalculateSystemTest {

    private iCalculateSystem calculateSystem = new CalculateSystem();


    @Test
    public void testGetResult() throws Exception {

        //Общий тест
        generalTest();

        //Тест подсчета с процентами
        percantageTest();


    }

    private void percantageTest() {

        AbstractExcel purchaseExcel = new PurchaseExcel(Paths.get("src", "test", "resources", "percentageTest", "purchase.xls").toFile());

        AbstractExcel monitorExcel = new MonitoringExcel(Paths.get("src" , "test", "resources", "percentageTest", "monitor.xls").toFile());

        calculateSystem.setMonitoring(monitorExcel);

        calculateSystem.setPurchase(purchaseExcel);


        iParametres parametres;
        Item item;
        parametres = new Parametres();

        parametres.setBottomMargin(200);//Commission > CommissionMin
        parametres.setTopMargin(1800);
        parametres.setCommissionMin(150);
        parametres.setCommissionPercentage(10);
        parametres.setConcurentPriceConsider(true);

        calculateSystem.setParametres(parametres);

        calculateSystem.getResult();

        item = calculateSystem.getItems().get(14);

        equalsName("nokia n85", item);
        equalsPrice(4183, item);//!! mistake. Commission - 3200/10=320. ConcurentPrice=4590; 4293-110+320 = 4503

        //тут, короче, получается так, что считается 4293 - 110 = 4183. Это связано с тем, что цена в данном случае не
        //должна быть меньше, чем у конкурента. Получается, что в цене наценка не учитывается, только при выборе
        //расчета цены.

        parametres = new Parametres();

        parametres.setBottomMargin(200);//Commission < CommissionMin
        parametres.setTopMargin(1800);
        parametres.setCommissionMin(300);
        parametres.setCommissionPercentage(3);
        parametres.setConcurentPriceConsider(true);

        calculateSystem.setParametres(parametres);
        calculateSystem.getResult();


        item = calculateSystem.getItems().get(11);
        equalsName("nokia x3-02", item);
        equalsPrice(4480, item);//Valid

        /*
        закупка - 3700
        с наценкой 3% - 3811
        цена конкурента - 4590
        разница - 779
        больше двухсот - цена = цена конкурента - 110

         */



        parametres = new Parametres();


        parametres.setBottomMargin(200);//Commission == CommissionMin
        parametres.setTopMargin(1800);
        parametres.setCommissionMin(135);
        parametres.setCommissionPercentage(3);
        parametres.setConcurentPriceConsider(true);

        calculateSystem.setParametres(parametres);
        calculateSystem.getResult();

        item = calculateSystem.getItems().get(6);
        equalsName("sony ericsson k850", item);

        equalsPrice(4889, item);//!! mistake.Commission - 4500/100*3=135. Use CommissionMin = 135.  ConcurentPrice=4999; 4999-110+135=5024

        // тоже самое. Получается, убрал подсчет комиссии после подсчета наценки, поэтому так считается.

        parametres = new Parametres();

        parametres.setBottomMargin(50);//Commission+1 == CommissionMin
        parametres.setTopMargin(1800);
        parametres.setCommissionMin(137);
        parametres.setCommissionPercentage(4);
        parametres.setConcurentPriceConsider(true);

        calculateSystem.setParametres(parametres);
        calculateSystem.getResult();


        item = calculateSystem.getItems().get(0);
        equalsName("nokia 2700", item);
        System.out.println(item.getPricePurchase());
        System.out.println(item.getPriceConcurrent());
        System.out.println(item.getPriceMinimum());
        equalsPrice(3577, item);//!! mistake.Commission - 3400/100*4=136. Use CommissionMin = 137.  ConcurentPrice=3640; 3400+40+137=3577

        /*
        тут интересней, такс
        цена 3400, комиссия 4% = 136. Минимальная - 137, получаем 3537

        Разница цены конкурента и закупки с комиссией - 3640 - 3537 = 103 - меньше двухсот
        считаем цена += нижний порог - 10 = 3537 + 50 - 10 = 3577

         */

        parametres = new Parametres();

        parametres.setBottomMargin(500);//Commission == CommissionMin+1
        parametres.setTopMargin(1800);
        parametres.setCommissionMin(194);
        parametres.setCommissionPercentage(3);
        parametres.setConcurentPriceConsider(true);

        calculateSystem.setParametres(parametres);
        calculateSystem.getResult();


        item = calculateSystem.getItems().get(5);
        equalsName("samsung galaxy grand neo gt-i9060 8gb", item);
        equalsPrice(8300, item);//!! mistake.Commission - 6500/100*3=195. Use Commission = 194.  ConcurentPrice=0; 6500+1800+195=8495
    }





    private void generalTest() {

        AbstractExcel purchaseExcel = new PurchaseExcel(Paths.get("src", "test", "resources", "purchase.xls").toFile());

        AbstractExcel monitorExcel = new MonitoringExcel(Paths.get("src" , "test", "resources", "monitor.xls").toFile());

        calculateSystem.setMonitoring(monitorExcel);

        calculateSystem.setPurchase(purchaseExcel);

        iParametres parametres = new Parametres();

        parametres.setBottomMargin(700);
        parametres.setTopMargin(1500);
        parametres.setCommissionMin(0);
        parametres.setCommissionPercentage(0);
        parametres.setConcurentPriceConsider(true);

        calculateSystem.setParametres(parametres);

        calculateSystem.getResult();

        List<Item> items = calculateSystem.getItems();

        Item item;

        item = items.get(0);
        assertEquals("nokia 2700", item.getName());
        assertEquals(4100, item.getPrice());
        assertEquals(false, item.isDifferentPurchasePrice());
        assertEquals(false, item.isMinimumPrice());
        assertEquals(3640, item.getPriceConcurrent());//

        item = items.get(1);
        assertEquals("nokia 2760", item.getName());
        assertEquals(3900, item.getPrice());
        assertEquals(false, item.isDifferentPurchasePrice());
        assertEquals(false, item.isMinimumPrice());
        assertEquals(0, item.getPriceConcurrent());

        item = items.get(2);
        assertEquals("nokia 3710 fold", item.getName());
        assertEquals(4780, item.getPrice());
        assertEquals(false, item.isDifferentPurchasePrice());
        assertEquals(true, item.isMinimumPrice());
        assertEquals(4890, item.getPriceConcurrent());

        item = items.get(3);
        assertEquals("nokia 1680 classic", item.getName());
        assertEquals(1970, item.getPrice());
        assertEquals(false, item.isDifferentPurchasePrice());
        assertEquals(false, item.isMinimumPrice());
        assertEquals(2080, item.getPriceConcurrent());

        item = items.get(4);
        assertEquals("nokia 5310", item.getName());
        assertEquals(3240, item.getPrice());
        assertEquals(false, item.isDifferentPurchasePrice());
        assertEquals(false, item.isMinimumPrice());
        assertEquals(3350, item.getPriceConcurrent());

        item = items.get(5);
        assertEquals("samsung galaxy grand neo gt-i9060 8gb", item.getName());
        assertEquals(8000, item.getPrice());
        assertEquals(false, item.isDifferentPurchasePrice());
        assertEquals(false, item.isMinimumPrice());
        assertEquals(0, item.getPriceConcurrent());

        /*item = items.get(6);
        assertEquals("sony ericsson k850", item.getName());
        assertEquals(4900, item.getPrice());
        assertEquals(false, item.isDifferentPurchasePrice());
        assertEquals(false, item.isMinimumPrice());
        assertEquals(4999, item.getPriceConcurrent());*/

        item = items.get(7);
        assertEquals("nokia 5800", item.getName());
        assertEquals(3900, item.getPrice());
        assertEquals(false, item.isDifferentPurchasePrice());
        assertEquals(true, item.isMinimumPrice());
        assertEquals(4500, item.getPriceConcurrent());

        item = items.get(8);
        assertEquals("nokia 6260", item.getName());
        assertEquals(4700, item.getPrice());
        assertEquals(true, item.isDifferentPurchasePrice());
        assertEquals(false, item.isMinimumPrice());
        assertEquals(4590, item.getPriceConcurrent());

        item = items.get(9);
        assertEquals("nokia n93i", item.getName());
        assertEquals(7300, item.getPrice());
        assertEquals(false, item.isDifferentPurchasePrice());
        assertEquals(true, item.isMinimumPrice());
        assertEquals(9000, item.getPriceConcurrent());

        item = items.get(10);
        assertEquals("nokia n95 8gb", item.getName());
        assertEquals(6000, item.getPrice());
        assertEquals(false, item.isDifferentPurchasePrice());
        assertEquals(true, item.isMinimumPrice());
        assertEquals(6799, item.getPriceConcurrent());

       item = items.get(11);
        assertEquals("nokia x3-02", item.getName());
        assertEquals(4480, item.getPrice());
        assertEquals(false, item.isDifferentPurchasePrice());
        assertEquals(false, item.isMinimumPrice());
        assertEquals(4590, item.getPriceConcurrent());

        item = items.get(12);
        assertEquals("nokia 6700 classic", item.getName());
        assertEquals(7580, item.getPrice());
        assertEquals(true, item.isDifferentPurchasePrice());
        assertEquals(true, item.isMinimumPrice());
        assertEquals(7690, item.getPriceConcurrent());

    }




    public void equalsPrice (int price, Item item)
    {
        assertEquals(price, item.getPrice());
    }
    public  void equalsName (String name, Item item)
    {
        assertEquals(name, item.getName());
    }


}
